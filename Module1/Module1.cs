﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module = new Module1();
            Console.WriteLine(module.GetMinimumValue(new int[] { 5, -4, -10, 3 }));
            int[] res = module.SwapItems(3, 5);
            Console.WriteLine(res[0] + " " + res[1]); 
        }


        public int[] SwapItems(int a, int b)
        {
            return new int[] { b, a };
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] < min)
                {
                    min = input[i];
                }
            }
            return min;
        }
    }
}

